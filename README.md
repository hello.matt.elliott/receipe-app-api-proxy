# Recipe app API proxy application

## Usage

### Env vars

- LISTEN_PORT - Nginx Port to listen on (default: 8000)
- APP_HOST - Hostname of the app to forward to (default: app)
- APP_PORT - Port of the app to forward to (default: 9000)
